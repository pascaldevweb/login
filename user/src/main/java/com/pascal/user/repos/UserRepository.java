package com.pascal.user.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pascal.user.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
    

}
