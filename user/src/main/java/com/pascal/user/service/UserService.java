package com.pascal.user.service;

import com.pascal.user.entities.Role;
import com.pascal.user.entities.User;

public interface UserService {
User saveUser(User user);
User findUserByUsername (String username);
Role addRole(Role role);
User addRoleToUser(String username, String rolename);
}