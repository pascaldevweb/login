package com.pascal.user.security;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@Configuration
public class SecurityConfig {
   
  

    // @Bean
    // public InMemoryUserDetailsManager user(){
    //     return new InMemoryUserDetailsManager(
    //         User.withUsername("pascal")
    //         .password("{noop}123")
    //         .authorities("read")
    //         .build()
    //     );
    // }



    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

                  return  http 
                    .csrf(csrf->csrf.disable())
                    .authorizeHttpRequests(auth-> auth.anyRequest().authenticated())
                    .httpBasic()
                    .and()
                    .addFilter(new JWTAuthenticationFilter(null)) 
                    .sessionManagement(session->session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                    .build();
    }

    

}